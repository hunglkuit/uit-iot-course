# IoT Engineering
 

## Lesson 5: Local Connectivity with Bluetooth LE
- [Slides](http://www.tamberg.org/fhnw/2020/hs/IoT05BluetoothLEConnectivity.pdf)
- [Handout](http://www.tamberg.org/fhnw/2020/hs/IoT05BluetoothLEConnectivityHandout.pdf)

## Examples
- [nRF52840_BeaconBleObservable.ino](Arduino/nRF52840_BeaconBleObservable/nRF52840_BeaconBleObservable.ino)
- [nRF52840_HrmBleCentral.ino](Arduino/nRF52840_HrmBleCentral/nRF52840_HrmBleCentral.ino)
- [nRF52840_HrmBlePeripheral.ino](Arduino/nRF52840_HrmBlePeripheral/nRF52840_HrmBlePeripheral.ino)
- [nRF52840_ScannerBleCentral.ino](Arduino/nRF52840_ScannerBleCentral/nRF52840_ScannerBleCentral.ino)
- [nRF52840_UartBleCentral.ino](Arduino/nRF52840_UartBleCentral/nRF52840_UartBleCentral.ino)
- [nRF52840_UartBlePeripheral.ino](Arduino/nRF52840_UartBlePeripheral/nRF52840_UartBlePeripheral.ino)

## Hands-on
- [Hands-on Lesson 5](../../../../fhnw-iot-work-05/blob/master/README.md)
