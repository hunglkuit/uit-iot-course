# IoT Engineering
 

## Lesson 11: Voice Control for Connected Products
- [Slides](http://www.tamberg.org/fhnw/2020/hs/IoT11VoiceControl.pdf)
- [Handout](http://www.tamberg.org/fhnw/2020/hs/IoT11VoiceControlHandout.pdf)

## Examples
- [DebuggerAlexaSkill](Nodejs/DebuggerAlexaSkill/index.js)
- [FishTankAlexaSkill](Nodejs/FishTankAlexaSkill/index.js)
- [SayHelloAlexaSkill](Nodejs/SayHelloAlexaSkill/index.js)

## Hands-on
- [Hands-on Lesson 11](../../../../fhnw-iot-work-11/blob/master/README.md)
